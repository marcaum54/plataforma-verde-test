Utilizei o **pipenv** para gerenciar minha virtualenv, caso não o tenha, instale-o [pipenv](https://pypi.org/project/pipenv/).

Depois de instalado, basta entrar na pasta do projeto e rodar: **pipenv shell** que a virtualenv será criada e todas as dependências serão instaladas.

Qualquer dúvida, basta entrar em contato: marcaum54@gmail.com
