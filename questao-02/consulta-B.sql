    SELECT u.id AS is_usuario,
           nome,
           c.id AS id_compra,
           c.valor AS valor_compra,
           c.item AS item_compra 
      FROM usuarios u 
 LEFT JOIN compras c ON c.id_usuario = u.id;