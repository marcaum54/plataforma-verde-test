from tinydb import TinyDB
from pathlib import Path

db_path = 'questao-03/usuarios.json'
db_file = Path(db_path)
if(not db_file.exists()):
    db_file.write_text('{}')
db = TinyDB(db_path)
