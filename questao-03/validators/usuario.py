def validator(request, db=None):
    errors = []
    status_code = 401

    is_create = request.method in ['POST']
    is_update = request.method in ['PUT', 'PATCH']
    is_delete = request.method in ['DELETE']

    if is_create or is_update:
        if not request.form.get('nome'):
            errors.append({'nome': 'é obrigatório'})

    if is_update or is_delete:
        id = request.view_args.get('id')
        exists = db.get(doc_id=id)
        if not exists:
            errors = f'Usuário "{id}" não existe'
            status_code = 404

    if not errors:
        if is_create:
            status_code = 201

        if is_update:
            status_code = 200

        if is_delete:
            status_code = 204

    is_valid = not bool(errors)

    return is_valid, errors, status_code
