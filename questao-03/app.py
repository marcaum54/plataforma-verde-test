from db import db

from flask import Flask, jsonify, request
from tinydb import Query

from validators.usuario import validator

app = Flask(__name__)


@app.route('/usuarios', methods=['GET'])
def index():
    usuarios = []

    for usuario in db.all():
        usuarios.append({'id': usuario.doc_id, 'nome': usuario.get('nome')})

    return jsonify(usuarios)


@app.route('/usuario', methods=['POST'])
def create():
    is_valid, retorno, status_code = validator(request)

    if is_valid:
        usuario = {'nome': request.form.get('nome')}
        retorno = db.insert(usuario)

    return jsonify(retorno), status_code


@app.route('/usuario/<int:id>', methods=['PATCH', 'PUT'])
def update(id):
    is_valid, retorno, status_code = validator(request, db)

    if is_valid:
        db.update({'nome': request.form.get('nome')}, doc_ids=[id])
        retorno = db.get(doc_id=id)

    return retorno, status_code


@app.route('/usuario/<int:id>', methods=['DELETE'])
def delete(id):
    is_valid, retorno, status_code = validator(request, db)

    if is_valid:
        db.remove(doc_ids=[id])
        retorno = ''

    return retorno, status_code
