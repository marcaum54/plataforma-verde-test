class Time(object):
    PONTOS_POR_EMPATE = 1
    PONTOS_POR_VITORIA = 3

    TIPO_RESULTADO_DERROTA = 'derrota'
    TIPO_RESULTADO_EMPATE = 'empate'
    TIPO_RESULTADO_VITORIA = 'vitória'

    def __init__(self, pontos=0, gols_pros=0, gols_contras=0, partidas_jogadas=0):
        self.pontos = pontos
        self.gols_pros = gols_pros
        self.gols_contras = gols_contras
        self.partidas_jogadas = partidas_jogadas

    def calcular_media_gols_por_partida(self):
        return self.gols_pros / self.partidas_jogadas

    def calcular_media_pontos_por_partida(self):
        return self.pontos / self.partidas_jogadas

    def calcular_saldo_de_gols(self):
        return self.gols_pros - self.gols_contras

    def add_partida(self, tipo_resultado, gols_pros, gols_contras):
        tipos = [
            self.TIPO_RESULTADO_VITORIA,
            self.TIPO_RESULTADO_EMPATE,
            self.TIPO_RESULTADO_DERROTA,
        ]

        assert tipo_resultado in tipos, 'Erro: O tipo resultado pode ser somente: "vitória", "empate", "derrota"'

        if tipo_resultado == self.TIPO_RESULTADO_VITORIA:
            self.pontos += 3

        if tipo_resultado == self.TIPO_RESULTADO_EMPATE:
            self.pontos += 1

        self.partidas_jogadas += 1
        self.gols_pros += gols_pros
        self.gols_contras += gols_contras

    def relatorio(self):
        return {
            'partidas_jogadas': self.partidas_jogadas,
            'pontos': self.pontos,
            'saldo_de_gols': self.calcular_saldo_de_gols(),
            'media_de_gols_por_partida': self.calcular_media_gols_por_partida(),
            'media_de_pontos_por_partida': self.calcular_media_pontos_por_partida(),
        }

#################### EXAMPLE ####################


# time = Time()

# time.add_partida(time.TIPO_RESULTADO_VITORIA, 5, 2)

# time.add_partida(time.TIPO_RESULTADO_DERROTA, 2, 10)

# time.add_partida(time.TIPO_RESULTADO_EMPATE, 1, 1)
# time.add_partida(time.TIPO_RESULTADO_EMPATE, 1, 1)
# time.add_partida(time.TIPO_RESULTADO_EMPATE, 1, 1)

# print(time.relatorio())
